import React from "react";
import { Link } from "react-router-dom";

import Logo from "../../assets/img/white-logo.png";

export const Header = () => {
  return (
    <>
      {/* Start Navbar Area */}
      <div className="navbar-area white-color">
        <div className="patoi-responsive-nav">
          <div className="container">
            <div className="patoi-responsive-menu">
              <div className="logo">
                <Link to="/">
                  <img src={Logo} alt="logo" />
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="patoi-nav">
          <div className="container">
            <nav className="navbar navbar-expand-md navbar-light">
              <Link className="navbar-brand" to="/">
                <img src={Logo} alt="logo" />
              </Link>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#menu"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse mean-menu" id="menu">
                <ul className="navbar-nav">
                  <li className="nav-item">
                    <Link to="/" className=" nav-link">
                      Inicio
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link
                      to="#"
                      className="dropdown-toggle nav-link"
                      id="dropdownMenuLink"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      Paginas
                    </Link>
                    <ul
                      className="dropdown-menu"
                      aria-labelledby="dropdownMenuLink"
                    >
                      <li className="nav-item">
                        <Link
                          to="about.html"
                          className="nav-link dropdown-item"
                        >
                          Acerca de Nosotros
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link
                          to="order-tracking.html"
                          className="nav-link dropdown-item"
                        >
                          Seguimiento de Pedido
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link to="faq.html" className="nav-link dropdown-item">
                          FAQ
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link
                          to="privacy-policy.html"
                          className="nav-link dropdown-item"
                        >
                          Política de Privacidad
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link
                          to="terms-conditions.html"
                          className="nav-link dropdown-item"
                        >
                          Términos y Condiciones
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link
                          to="not-found.html"
                          className="nav-link dropdown-item"
                        >
                          Página de Error 404
                        </Link>
                      </li>
                    </ul>
                  </li>
                  <li className="nav-item">
                    <Link
                      to="#"
                      className="dropdown-toggle nav-link"
                      id="dropdownMenuLink"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      Tienda
                    </Link>
                    <ul className="dropdown-menu">
                      <li className="nav-item">
                        <Link to="shop-grid.html" className="nav-link">
                          Tienda de Cuadrícula
                        </Link>
                      </li>

                      <li className="nav-item">
                        <Link to="products-details.html" className="nav-link">
                          Detalles de Producto
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link to="/cartshopping" className="nav-link">
                          Carrito
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link to="checkout.html" className="nav-link">
                          Pedido
                        </Link>
                      </li>
                      <li className="nav-item">
                        <Link to="/whislist" className="nav-link">
                          Lista de Deseos
                        </Link>
                      </li>
                    </ul>
                  </li>

                  <li className="nav-item">
                    <Link to="/contactus" className="nav-link">
                      Contacto
                    </Link>
                  </li>
                </ul>
                <div className="others-option">
                  <div className="d-flex align-items-center">
                    <ul>
                      <li>
                        <Link to="/myaccount">
                          <i className="bx bx-user-circle"></i>
                        </Link>
                      </li>
                      <li>
                        <Link to="/cartshopping">
                          <i className="bx bx-cart"></i>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </nav>
          </div>
        </div>
        <div className="others-option-for-responsive">
          <div className="container">
            <div className="dot-menu">
              <div className="inner">
                <div className="circle circle-one"></div>
                <div className="circle circle-two"></div>
                <div className="circle circle-three"></div>
              </div>
            </div>
            <div className="container">
              <div className="option-inner">
                <div className="others-option">
                  <ul>
                    <li>
                      <Link to="/myaccount">
                        <i className="bx bx-user-circle"></i>
                      </Link>
                    </li>
                    <li>
                      <Link to="/cartshopping">
                        <i className="bx bx-cart"></i>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* End Navbar Area */}
    </>
  );
};
