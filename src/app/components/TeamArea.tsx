import React from "react";

import team1 from "../../assets/img/team/team1.jpg";
import team2 from "../../assets/img/team/team2.jpg";
import team3 from "../../assets/img/team/team3.jpg";
import team4 from "../../assets/img/team/team4.jpg";
import team5 from "../../assets/img/team/team5.jpg";

export default function TeamArea() {
  return (
    <>
      <div className="team-area pb-75">
        <div className="container">
          <div className="section-title">
            <h2>Meet Our Founders</h2>
          </div>
          <div className="row justify-content-center">
            <div className="col-lg-4 col-md-6 col-sm-6">
              <div className="single-team-member">
                <img src={team1} alt="team-image" />
                <h3>Esther Ortegon</h3>
                <span>Scrum Master</span>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-6">
              <div className="single-team-member">
                <img src={team2} alt="team-image" />
                <h3>Ivan Carvajal</h3>
                <span>Desarrollador</span>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-6">
              <div className="single-team-member">
                <img src={team3} alt="team-image" />
                <h3>Ivan Gonzale</h3>
                <span>Project Manager</span>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-6">
              <div className="single-team-member">
                <img src={team4} alt="team-image" />
                <h3>Maria Sierra</h3>
                <span>Desarrolladora</span>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-6">
              <div className="single-team-member">
                <img src={team5} alt="team-image" />
                <h3>Octavio Torres</h3>
                <span>Desarrollador</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
