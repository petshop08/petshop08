import React from 'react'
import { Link } from "react-router-dom";
import products4 from "../../assets/img/products/products4.jpg";
import products5 from "../../assets/img/products/products5.jpg";
import products6 from "../../assets/img/products/products6.jpg";
import products7 from "../../assets/img/products/products7.jpg";


export default function WishList() {
  return (
    <>
    <div className="wishlist-area ptb-100">
            <div className="container">
                <form>
                    <div className="wishlist-table table-responsive">
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Unit Price</th>
                                    <th scope="col">Stock Status</th>
                                    <th scope="col">Shop Now</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>

                                <Link to="products-details.html">
                                            <img src={products4} alt="item"/>
                                        </Link>
                                    </td>
                                    <td><Link to="products-details.html">Pet chair</Link></td>
                                    <td className="product-price">$150.00</td>
                                    <td className="product-stock-status">
                                        <span className="in-stock"><i className='bx bx-check-circle'></i> In Stock</span>
                                    </td>
                                    <td>
                                        <Link to="cart.html" className="default-btn"><span>Shop Now</span></Link>
                                    </td>
                                    <td><Link to="wishlist.html" className="remove"><i className='bx bx-trash-alt'></i></Link></td>
                                </tr>
                                <tr>
                                    <td>
                                        <Link to="products-details.html">
                                            <img src={products5} alt="item"/>
                                        </Link>
                                    </td>
                                    <td><Link to="products-details.html">Pink ceramic cat bowl</Link></td>
                                    <td className="product-price">$199.00</td>
                                    <td className="product-stock-status">
                                        <span className="in-stock"><i className='bx bx-check-circle'></i> In Stock</span>
                                    </td>
                                    <td>
                                        <Link to="cart.html" className="default-btn"><span>Shop Now</span></Link>
                                    </td>
                                    <td><Link to="wishlist.html" className="remove"><i className='bx bx-trash-alt'></i></Link></td>
                                </tr>
                                <tr>
                                    <td>
                                        <Link to="products-details.html">
                                            <img src={products6} alt="item"/>
                                        </Link>
                                    </td>
                                    <td><Link to="products-details.html">Pet carrier</Link></td>
                                    <td className="product-price">$233.99</td>
                                    <td className="product-stock-status">
                                        <span className="in-stock"><i className='bx bx-check-circle'></i> In Stock</span>
                                    </td>
                                    <td>
                                        <Link to="cart.html" className="default-btn"><span>Shop Now</span></Link>
                                    </td>
                                    <td><Link to="wishlist.html" className="remove"><i className='bx bx-trash-alt'></i></Link></td>
                                </tr>
                                <tr>
                                    <td>
                                        <Link to="products-details.html">
                                            <img src={products7} alt="item"/>
                                        </Link>
                                    </td>
                                    <td><Link to="products-details.html">Red dog bed</Link></td>
                                    <td className="product-price">$223.00</td>
                                    <td className="product-stock-status">
                                        <span className="out-stock"><i className='bx bx-x'></i> Out of Stock</span>
                                    </td>
                                    <td>
                                        <Link to="cart.html" className="default-btn"><span>Shop Now</span></Link>
                                    </td>
                                    <td><Link to="wishlist.html" className="remove"><i className='bx bx-trash-alt'></i></Link></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </>
  )
}
