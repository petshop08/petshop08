import React from "react";
  
export const ContactUs = () => {
  return (
    <div className="contact-area pt-100">
    <div className="container">
        <div className="row">
            <div className="col-lg-8 col-md-12">
                <div className="contact-form">
                    <h3>Get In Touch</h3>
                    <form id="contactForm">
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6">
                                <div className="form-group mb-3">
                                    <label>Your Name</label>
                                    <input type="text" name="name" className="form-control" id="name" required data-error="Please enter your name"/>
                                    <div className="help-block with-errors"></div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6">
                                <div className="form-group mb-3">
                                    <label>Email Address</label>
                                    <input type="email" name="email" className="form-control" id="email" required data-error="Please enter your email"/>
                                    <div className="help-block with-errors"></div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6">
                                <div className="form-group mb-3">
                                    <label>Phone Number</label>
                                    <input type="text" name="phone_number" className="form-control" id="phone_number" required data-error="Please enter your phone number"/>
                                    <div className="help-block with-errors"></div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6">
                                <div className="form-group mb-3">
                                    <label>Subject</label>
                                    <input type="text" name="msg_subject" className="form-control" id="msg_subject" required data-error="Please enter your subject"/>
                                    <div className="help-block with-errors"></div>
                                </div>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12">
                                <div className="form-group mb-3">
                                    <label>Message...</label>
                                    <textarea name="message" id="message" className="form-control" required data-error="Please enter your message"></textarea>
                                    <div className="help-block with-errors"></div>
                                </div>
                            </div>
                            
                            <div className="col-lg-12 col-md-12 col-sm-12 mb-4">
                                <button type="submit" className="default-btn"><span>Send Message</span></button>
                                <div id="msgSubmit" className="h3 text-center hidden"></div>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div className="col-lg-4 col-md-12">
                <div className="contact-info">
                    <h3>Contact Information</h3>
                    <ul>
                        <li><span>Hotline:</span> <a href="tel:12855">12855</a></li>
                        <li><span>Tech support:</span> <a href="tel:+1514312-5678">+1 (514) 312-5678</a></li>
                        <li><span>Email:</span> <a href="mailto:hello@patoi.com">hello@patoi.com</a></li>
                        <li><span>Address:</span> 1523 Cook Hill Road New Haven, CT</li>
                        <li><span>Available:</span> Monday - Friday 8:00am - 8:00pm</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
  );
};
