import React from 'react'
import icon1 from "../../assets/img/icon/icon1.png";
import icon2 from "../../assets/img/icon/icon2.png";
import icon3 from "../../assets/img/icon/icon3.png";
import icon4 from "../../assets/img/icon/icon4.png";

export default function FacilityArea() {
  return (
    <>
    <div className="facility-area pb-75">
            <div className="container">
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <div className="facility-box">
                            <img src={icon1} alt="icon"/>
                            <h3>Best collection</h3>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <div className="facility-box bg-fed3d1">
                            <img src={icon2} alt="icon"/>
                            <h3>Fast delivery</h3>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <div className="facility-box bg-a9d4d9">
                            <img src={icon3} alt="icon"/>
                            <h3>24/7 customer support</h3>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 col-6">
                        <div className="facility-box bg-fef2d1">
                            <img src={icon4} alt="icon"/>
                            <h3>Secured payment</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>
  )
}
