import React from "react";

import products4 from "../../assets/img/products/products4.jpg";
import products5 from "../../assets/img/products/products5.jpg";
import products6 from "../../assets/img/products/products6.jpg";
import products7 from "../../assets/img/products/products7.jpg";
import products8 from "../../assets/img/products/products8.jpg";
import user1 from "../../assets/img/user/user1.jpg";
import user2 from "../../assets/img/user/user2.jpg";
import user3 from "../../assets/img/user/user3.jpg";
import user4 from "../../assets/img/user/user4.jpg";

export const ProductsDetails = () => {
    return (
        <div>
            <div className="products-details-area ptb-100">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-5 col-md-12">
                        <div className="products-details-thumbs-image">
                            <ul className="products-details-thumbs-image-slides">
                            
                                <li><img src={products4} alt="image"/></li>
                                <li><img src={products5} alt="image"/></li>
                                <li><img src={products6} alt="image"/></li>
                                <li><img src={products7} alt="image"/></li>
                                <li><img src={products8} alt="image"/></li>
                            </ul>
                            <div className="slick-thumbs">
                                <ul>
                                    <li><img src={products4} alt="image"/></li>
                                    <li><img src={products5} alt="image"/></li>
                                    <li><img src={products6} alt="image"/></li>
                                    <li><img src={products7} alt="image"/></li>
                                    <li><img src={products8} alt="image"/></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-7 col-md-12">
                        <div className="products-details-desc">
                            <h3>Stack pet collars</h3>
                            <div className="price">
                                <span className="new-price">$35.00</span>
                                <span className="old-price">$55.00</span>
                            </div>
                            <div className="rating">
                                <i className='bx bxs-star'></i>
                                <i className='bx bxs-star'></i>
                                <i className='bx bxs-star'></i>
                                <i className='bx bxs-star'></i>
                                <i className='bx bxs-star'></i>
                            </div>
                            <p>Santiago who travels from his homeland in Spain to the Egyptian desert in search of a treasure buried near the Pyramids. Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor incididunt ut labore et. Lorem ipsum dolor sit amet, consectetur adipiscing elitet.</p>
                            <div className="products-add-to-cart">
                                <div className="input-counter">
                                    <span className="minus-btn"><i className='bx bx-minus'></i></span>
                                    <input type="text" value="1"/>
                                    <span className="plus-btn"><i className='bx bx-plus'></i></span>
                                </div>
                                <button type="submit" className="default-btn"><span>Add to Cart</span></button>
                            </div>
                            <a href="wishlist.html" className="add-to-wishlist"><i className='bx bx-heart'></i> Add to wishlist</a>
                            <ul className="products-info">
                                <li><span>SKU:</span> 007</li>
                                <li><span>Categories:</span> <a href="shop-grid.html">Brash</a></li>
                                <li><span>Availability:</span> In stock (7 items)</li>
                                <li><span>Tag:</span> Accessories</li>
                            </ul>
                            <div className="products-share">
                                <ul className="social">
                                    <li><span>Share:</span></li>
                                    <li><a href="#" className="facebook" target="_blank"><i className='bx bxl-facebook'></i></a></li>
                                    <li><a href="#" className="twitter" target="_blank"><i className='bx bxl-twitter'></i></a></li>
                                    <li><a href="#" className="linkedin" target="_blank"><i className='bx bxl-linkedin'></i></a></li>
                                    <li><a href="#" className="instagram" target="_blank"><i className='bx bxl-instagram'></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 col-md-12">
                        <div className="products-details-tabs">
                            <ul className="nav nav-tabs" id="myTab" role="tablist">
                                <li className="nav-item">
                                    <button className="nav-link active" id="description-tab" data-bs-toggle="tab" data-bs-target="#description" type="button" role="tab" aria-controls="description" aria-selected="false">Description</button>
                                </li>
                                <li className="nav-item">
                                    <button className="nav-link" id="additional-information-tab" data-bs-toggle="tab" data-bs-target="#additional-information" type="button" role="tab" aria-controls="additional-information" aria-selected="false">Additional Info</button>
                                </li>
                                <li className="nav-item">
                                    <button className="nav-link" id="reviews-tab" data-bs-toggle="tab" data-bs-target="#reviews" type="button" role="tab" aria-controls="reviews" aria-selected="false">Reviews</button>
                                </li>
                            </ul>
                            <div className="tab-content" id="myTabContent">
                                <div className="tab-pane fade show active" id="description" role="tabpanel">
                                    <p>This story, dazzling in its powerful simplicity and soul-stirring wisdom, is about an Andalusian shepherd boy named Santiago who travels from his homeland in Spain to the Egyptian desert in search of a treasure buried near the Pyramids. Lorem ipsum dolor sit.</p>
                                    <ul>
                                        <li>Instant <strong>Patoi</strong> bestseller</li>
                                        <li>Translated into 18 languages</li>
                                        <li>#1 Most Recommended Book of the year.</li>
                                        <li>A neglected project, widely dismissed, its champion written off as unhinged.</li>
                                        <li>Yields a negative result in an experiment because of a flaw in the design of the experiment.</li>
                                        <li>An Amazon, Bloomberg, Financial Times, Forbes, Inc., Newsweek, Strategy + Business, Tech Crunch, Washington Post Best Business Book of the year</li>
                                    </ul>
                                    <p><i>This story, dazzling in its powerful simplicity and soul-stirring wisdom, is about an Andalusian shepherd boy named Santiago who travels from his homeland in Spain to the Egyptian desert in search of a treasure buried near the Pyramids. Lorem ipsum dolor sit.</i></p>
                                </div>
                                <div className="tab-pane fade" id="additional-information" role="tabpanel">
                                    <div className="table-responsive">
                                        <table className="table table-striped">
                                            <tbody>
                                                <tr>
                                                    <td>Handle Height</td>
                                                    <td>40-45″</td>
                                                </tr>
                                                <tr>
                                                    <td>Width</td>
                                                    <td>24″</td>
                                                </tr>
                                                <tr>
                                                    <td>Wheels</td>
                                                    <td>12″</td>
                                                </tr>
                                                <tr>
                                                    <td>Dimensions</td>
                                                    <td>10 × 10 × 10 cm</td>
                                                </tr>
                                                <tr>
                                                    <td>Material</td>
                                                    <td>Iron</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="reviews" role="tabpanel">
                                    <div className="products-review-comments">
                                        <div className="user-review">
                                            <img src={user1} alt="image"/>
                                            <div className="review-rating">
                                                <div className="review-stars">
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                </div>
                                            </div>
                                            <span className="d-block sub-name">James Anderson</span>
                                            <p>Very well built theme, couldn't be happier with it. Can't wait for future updates to see what else they add in.</p>
                                        </div>
                                        <div className="user-review">
                                            <img src={user2} alt="image"/>
                                            <div className="review-rating">
                                                <div className="review-stars">
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star'></i>
                                                    <i className='bx bxs-star'></i>
                                                </div>
                                            </div>
                                            <span className="d-block sub-name">Sarah Taylor</span>
                                            <p>Santiago who travels from his homeland in Spain to the Egyptian desert in search of a treasure buried near the Pyramids. Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor incididunt ut labore et. Lorem ipsum dolor sit amet, consectetur adipiscing elitet.</p>
                                        </div>
                                        <div className="user-review">
                                            <img src={user3} alt="image"/>
                                            <div className="review-rating">
                                                <div className="review-stars">
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                </div>
                                            </div>
                                            <span className="d-block sub-name">David Warner</span>
                                            <p>Stunning design, very dedicated crew who welcome new ideas suggested by customers, nice support.</p>
                                        </div>
                                        <div className="user-review">
                                            <img src={user4} alt="image"/>
                                            <div className="review-rating">
                                                <div className="review-stars">
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star checked'></i>
                                                    <i className='bx bxs-star'></i>
                                                </div>
                                            </div>
                                            <span className="d-block sub-name">King Kong</span>
                                            <p>Stunning design, very dedicated crew who welcome new ideas suggested by customers, nice support.</p>
                                        </div>
                                    </div>
                                    <div className="review-form-wrapper">
                                        <h3>Add a review</h3>
                                        <p className="comment-notes">Your email address will not be published. Required fields are marked <span>*</span></p>
                                        <form>
                                            <div className="row">
                                                <div className="col-lg-12 col-md-12">
                                                    <div className="rating">
                                                        <input type="radio" id="star5" name="rating" value="5" /><label htmlFor="star5"></label>
                                                        <input type="radio" id="star4" name="rating" value="4" /><label htmlFor="star4"></label>
                                                        <input type="radio" id="star3" name="rating" value="3" /><label htmlFor="star3"></label>
                                                        <input type="radio" id="star2" name="rating" value="2" /><label htmlFor="star2"></label>
                                                        <input type="radio" id="star1" name="rating" value="1" /><label htmlFor="star1"></label>
                                                    </div>
                                                </div>
                                                <div className="col-lg-6 col-md-6">
                                                    <div className="form-group">
                                                        <input type="text" className="form-control" placeholder="Name *"/>
                                                    </div>
                                                </div>
                                                <div className="col-lg-6 col-md-6">
                                                    <div className="form-group">
                                                        <input type="email" className="form-control" placeholder="Email *"/>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-md-12">
                                                    <div className="form-group">
                                                        <textarea placeholder="Your review" className="form-control" cols={30} rows={6}></textarea>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 col-md-12">
                                                    <p className="comment-form-cookies-consent">
                                                        <input type="checkbox" id="test1"/>
                                                        <label htmlFor="test1">Save my name, email, and website in this browser for the next time I comment.</label>
                                                    </p>
                                                </div>
                                                <div className="col-lg-12 col-md-12">
                                                    <button type="submit">SUBMIT</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    )
}