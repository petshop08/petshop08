import React from 'react'
import { Link } from "react-router-dom";

interface propsTitleArea{
  title: string;
}

export const TitleArea = ({
  title
}
: propsTitleArea) => {
  return (
    <>
    <div className="page-title-area">
      <div className="container">
        <div className="page-title-content">
          <h1>{title}</h1>
          <ul>
            <li>
              <Link to="/">Pagina Principal</Link>
            </li>
            <li>{title}</li>
          </ul>
        </div>
      </div>
    </div>
  </>
  )
}

