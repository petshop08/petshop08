import React from "react";

import prod1 from "../../assets/img/products/products9.jpg";

import { Link } from "react-router-dom";

export const Cart = () => {
	const actualizar = () => {
		// eslint-disable-next-line
		location.reload();
	}
	return (
		<div className="cart-area ptb-100">
			<div className="container">
				<form>
					<div className="cart-table table-responsive">
						<table className="table table-bordered">
							<thead>
								<tr>
									<th scope="col"></th>
									<th scope="col">Producto</th>
									<th scope="col">Precio</th>
									<th scope="col">Cantidad</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<Link to="/cartshopping" className="remove">
										<i className="fa-solid fa-trash-can"></i>
										</Link>
									</td>
									<td className="product-thumbnail">
										<Link to="products-details.html">
											<img src={prod1} alt="item" />
											<h3>Pet chair</h3>
										</Link>
									</td>
									<td>$199.00</td>
									<td className="product-quantity">
										<div className="input-counter">
											<span className="minus-btn">
												<i className="bx bx-minus"></i>
											</span>
											<input type="text" value="1" />
											<span className="plus-btn">
												<i className="bx bx-plus"></i>
											</span>
										</div>
									</td>
									<td>$199.00</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div className="cart-buttons">
						<div className="row align-items-center">
							<div className="col-lg-5 col-sm-12 col-md-5 text-end">
								<span
								 className="default-btn"
								 onClick={actualizar}
								 >
									<span>Actualizar Carrito</span>
								</span>
							</div>
						</div>
					</div>
					<div className="cart-totals">
						<ul>
							<li>
								Subtotal <span>$800.00</span>
							</li>
							<li>
								Envio <span>$30.00</span>
							</li>
							<li>
								Total <span>$830.00</span>
							</li>
						</ul>
						<Link to="#" className="default-btn">
							<span>Comprar</span>
						</Link>
					</div>
				</form>
			</div>
		</div>
	);
};
