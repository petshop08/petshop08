import React from "react";
import { Cart } from "../../components/Cart";
import { TitleArea } from "../../components/TitleArea";

export const CartShopping = () => {
  return (
  <>
  <TitleArea
    title = "Carrito"
  />
  <Cart/>
  </>);
};
