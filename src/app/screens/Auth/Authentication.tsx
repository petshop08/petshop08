import React from "react";
import { TitleArea } from "../../components/TitleArea";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { FormLogin } from "./FormLogin";
import { FormRegister } from "./FormRegister";

export const Authentication = () => {
  
  return (
    <>
      <TitleArea
      title = "Ingreso"
      />
      <div className="profile-authentication-area ptb-100">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-12">
              <FormLogin />
            </div>

            <div className="col-lg-6 col-md-12">
              <FormRegister />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};


