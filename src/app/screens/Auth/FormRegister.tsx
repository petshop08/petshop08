import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";

//definir estructura del formulario mediante TYPES
type UserSubmitForm = {
  nombreCompleto: string;
  nombreUsuario: string;
  correo: string;
  numeroTelefono: number;
  contraseña: string;
  confirmarContraseña: string;
};

export const FormRegister = () => {

  //definir esquema a usar en el form con la libreria yup y sus funciones
  const validationSchema = Yup.object().shape({
    nombreCompleto: Yup.string().required("El nombre es obligatorio"),
    nombreUsuario: Yup.string()
      .required("Nombre de usuario")
      .min(8, "El nombre de usuario debe tener al menos 8 caracteres")
      .max(20, "El nombre de usuario no debe exceder los 20 caracteres"),
    correo: Yup.string().required("correo electronico es requerido").email("el correo electrónico es invalido"),
    numeroTelefono: Yup.number()
    .typeError('debes especificar un numero de telefono valido')
    .required("telefono es requerido")
    .min(7, "el numero de telefono debe tener al menos 7 caracteres")
    .max(10, "el numero de Telefono no debe exceder los 10 caracteres"),
    contraseña: Yup.string()
      .required("Se requiere contraseña")
      .min(8, "La contraseña debe tener al menos 8 caracteres")
      .max(40, "La contraseña no debe exceder los 40 caracteres"),
    confirmarContraseña: Yup.string()
      .required("Se requiere confirmar contraseña")
      .oneOf([Yup.ref("contraseña"), null], "Las contraseñas no coinciden")
  });
//desestructuracion de metodos a usar en el form
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<UserSubmitForm>({
    resolver: yupResolver(validationSchema),
  });

  // si se da submit se muestra en consola si las validaciones se cumplen
  const onSubmit = (data: UserSubmitForm) => {
    console.log(JSON.stringify(data, null, 2));
  };
  return (
    <>
      <div className="register-form">
        <h2>Registrar</h2>
        <form onSubmit={handleSubmit(onSubmit)} noValidate>
          <div className="form-group">
            <label>Nombre Completo</label>
            <input
              type="text"
              {...register('nombreCompleto')}
              className={`form-control ${errors.nombreCompleto ? 'is-invalid' : ''}`}
              placeholder="Nombre Completo"
              required
            />
            <div className="invalid-feedback">{errors.nombreCompleto?.message}</div>
          </div>
          <div className="form-group">
            <label>Nombre de Usuario</label>
            <input
              {...register('nombreUsuario')}
              type="text"
              className={`form-control ${errors.nombreUsuario ? 'is-invalid' : ''}`}
              placeholder="Nombre de Usuario"
              required
            />
            <div className="invalid-feedback">{errors.nombreUsuario?.message}</div>
          </div>
          <div className="form-group">
            <label>Correo Electronico</label>
            <input
              type="text"
              {...register('correo')}
              className={`form-control ${errors.correo ? 'is-invalid' : ''}`}
              placeholder="Nombre Completo"
              required
            />
            <div className="invalid-feedback">{errors.correo?.message}</div>
          </div>
          <div className="form-group">
            <label>Numero de Telefono</label>
            <input
              type="text"
              {...register('numeroTelefono')}
              className={`form-control ${errors.numeroTelefono ? 'is-invalid' : ''}`}
              placeholder="Numero de telefono o celular"
              required
            />
            <div className="invalid-feedback">{errors.numeroTelefono?.message}</div>
          </div>
          <div className="form-group">
            <label>Contraseña</label>
            <input
              type="password"
              {...register('contraseña')}
              className={`form-control ${errors.contraseña ? 'is-invalid' : ''}`}
              placeholder="contraseña"
              required
            />
            <div className="invalid-feedback">{errors.contraseña?.message}</div>
          </div>
          <div className="form-group">
            <label>Repetir Contraseña</label>
            <input
              type="password"
              {...register('confirmarContraseña')}
              className={`form-control ${errors.confirmarContraseña ? 'is-invalid' : ''}`}
              placeholder="confirmar contraseña"
              required
            />
            <div className="invalid-feedback">{errors.confirmarContraseña?.message}</div>
          </div>
          <p className="description">
            La contraseña debe tener al menos ocho caracteres. Para hacerla más
            fuerte, utilice letras mayúsculas y minúsculas, números y símbolos
            ¡como ! " ? $ % ^ & )
          </p>
          <button type="submit">Registrar</button>
        </form>
      </div>
    </>
  );
};
