import React from "react";
import { AboutArea } from "../../components/AboutArea";
import { AboutUs } from "../../components/AboutUs";
import FacilityArea from "../../components/FacilityArea";
import TeamArea from "../../components/TeamArea";

export const AboutUsScreen = () => {
  return (
    <>
      <AboutArea />
      <FacilityArea/>
      <TeamArea/>
    </>
  );
};
