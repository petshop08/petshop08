import { Route, Routes } from "react-router-dom";
import { PageDoesntExist } from "../../screens/404NotFound/PageDoesntExist";
import { AboutUsScreen } from "../../screens/AboutUs/AboutUsScreen";
import { Authentication } from "../../screens/Auth/Authentication";
import { CartShopping } from "../../screens/cart/Cart";
import { ContactScreen } from "../../screens/contact/ContactScreen";
import { Home } from "../../screens/Home/Home";
import { ProductsDetailsScreen } from "../../screens/ProductsDetails/ProductsDetailsScreen";
import { WhisListScreen } from "../../screens/whislist/WhisListScreen";


export const Rutes = () => {
  return (
    <Routes>
      <Route path="*" element={<PageDoesntExist/>} />

      <Route path="/myaccount" element={<Authentication />} />
      <Route path="/" element={<Home/>} />
      <Route path="/404Error" element={<PageDoesntExist/>} />
      <Route path="/cartshopping" element={<CartShopping/>} />
      <Route path="/contactus" element={<ContactScreen/>} />
      <Route path="/aboutus" element={<AboutUsScreen/>} />
      <Route path="/productdetails" element={<ProductsDetailsScreen/>} />
      <Route path="/whislist" element={<WhisListScreen/>} />


    </Routes>
  );
};